/**
 * +----------------------------------------------------------------------
 * | xystarter-uni [ 渐进式前台通用uni-app脚手架 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018-2020 http://jiangruyi.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Author: jry <598821125@qq.com>
 * +----------------------------------------------------------------------
 * | 版权申明：此源码不是一个自由软件，是jry推出的私有源码，严禁在未经许可的情况下
 * | 拷贝、复制、传播、使用此源码的任意代码，如有违反，请立即删除，否则您将面临承担相应
 * | 法律责任的风险。如果需要取得官方授权，请联系官方QQ598821125。
 */

import util from '../../libs/util';
let defaultUserInfo = {
    id: 0,
    nickname: '',
    username: '',
    avatar: 'https://i.loli.net/2019/06/28/5d15d0889f07857773.png',
    email: {
        identity_group: '',
        identifier: '',
        verified: 0
    },
    mobile: {
        identity_group: '',
        identifier: '',
        verified: 0
    },
    role: []
};
export default {
    namespaced: true,
    state: {
        token: '',
		userAgreement: 0,
        userInfo: defaultUserInfo
    },
    getters: {},
    mutations: {
        setData: (state, data) => {
            state[data.key] = data.value;
        },
        setToken: (state, data) => {
            state.token = data;
        },
        setUserInfo: (state, data) => {
            if (!data) {
                data = defaultUserInfo;
            }
            state.userInfo = data;
        }
    },
    actions: {
        setToken({ commit }, data) {
            commit('setToken', data);
        },
        setUserInfo({ commit }, data) {
            commit('setUserInfo', data);
        },
    }
}
