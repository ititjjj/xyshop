/**
 * +----------------------------------------------------------------------
 * | InitAdmin/vue-admin [ InitAdmin渐进式模块化通用后台 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018-2019 http://initadmin.net All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * +----------------------------------------------------------------------
 * | Author: jry <ijry@qq.com>
 * +----------------------------------------------------------------------
*/

import util from '../../libs/util';
export default {
  namespaced: true,
  state: {
	version: '0.2.0',
	runEnv: '', // 运行环境
	baseUrl: util.baseUrl(),
	createTime: Date.parse(new Date()) / 1000,
	updateTime: Date.parse(new Date()) / 1000,
	siteInfo: {
		title: 'XYStarter',
		logo: '/static/logo.png',
		logoTitle: '/static/logo.png',
		slogan: '',
		description: '',
		support: '技术支持｜XYCloud',
		copyright: 'Copyright © 2018-2019 jry All Rights Reserved',
		icp: '苏ICP备18067203号',
		exTheme: '',
		iconfig: {},
		versionFuncsAll: [],
		appDownurl: '',
	},
  },
  getters: {
  },
  mutations: {
	setData: (state, data) => {
		state[data.key] = data.value;
	},
	setRunEnv: (state, data) => {
		state.runEnv = data
	},
	setBaseUrl: (state, data) => {
		state.baseUrl = data
	},
	setSiteInfo:(state, data)=>{
	  state.siteInfo = data
	}
  },
  actions: {
	setSiteInfo({commit}, data){
	  commit('setSiteInfo', data)
	}
  }
}
