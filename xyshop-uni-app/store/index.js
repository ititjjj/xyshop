/**
 * +----------------------------------------------------------------------
 * | xystarter-uni [ 渐进式前台通用uni-app脚手架 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018-2020 http://jiangruyi.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Author: jry <598821125@qq.com>
 * +----------------------------------------------------------------------
 * | 版权申明：此源码不是一个自由软件，是jry推出的私有源码，严禁在未经许可的情况下
 * | 拷贝、复制、传播、使用此源码的任意代码，如有违反，请立即删除，否则您将面临承担相应
 * | 法律责任的风险。如果需要取得官方授权，请联系官方QQ598821125。
*/

import Vue from 'vue';
import Vuex from 'vuex';

import app from './module/app';
import user from './module/user';

import createPersistedState from 'vuex-persistedstate';
import {localStorage} from 'mp-storage';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    //
  },
  plugins: [
    createPersistedState({
      storage: localStorage
    })
  ],
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    app,
    user
  }
})
