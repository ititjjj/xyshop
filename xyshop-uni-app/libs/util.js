/**
 * +----------------------------------------------------------------------
 * | InitAdmin/vue-admin [ InitAdmin渐进式模块化通用后台 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018-2019 http://initadmin.net All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * +----------------------------------------------------------------------
 * | Author: jry <ijry@qq.com>
 * +----------------------------------------------------------------------
*/

import vue from '../main.js';
import store from '../store';
import util from '../components/xyui/src/util/util.js';

// 获取用户信息
util.getUserInfo = function () {
	let userInfo = uni.getStorageSync('userInfo');
	if (!userInfo) {
		return {
			id: 0,
			nickname: '',
			username: '',
			avatar: 'https://i.loli.net/2019/06/28/5d15d0889f07857773.png',
			role: []
		};
	}
    return userInfo;
};

// 存储用户信息
util.setUserInfo = function (data) {
	if (!data) {
		return uni.setStorageSync('userInfo', {
			id: 0,
			nickname: '',
			username: '',
			avatar: 'https://i.loli.net/2019/06/28/5d15d0889f07857773.png',
			role: []
		});
	}
	return uni.setStorageSync('userInfo', data);
};

// wsUrl
util.wsUrl = function () {
	if (process.env.NODE_ENV === 'development') {
		return 'ws://127.0.0.1:9508';
	}
	if (process.env.NODE_ENV === 'production') {
		return 'wss://xyshop.jiangruyi.com/wss/';
	}
};

// baseDomain
util.baseDomain = function () {
	if (process.env.NODE_ENV === 'development') {
		// #ifdef APP-PLUS || MP
		return 'https://xyshop.jiangruyi.com';
		// #endif
		return 'http://t9.test.com';
	}
	if (process.env.NODE_ENV === 'production') {
		// #ifdef H5
		return window.location.protocol + "//" + window.location.host;
		// #endif
		return 'https://xyshop.jiangruyi.com';
	}
};

// baseUrl
util.baseUrl = function () {
	let baseUrl = uni.getStorageSync('baseUrl');
	if (!baseUrl) {
		baseUrl = util.baseDomain() + '/api/';
	}
	return baseUrl;
};
util.setBaseUrl = function (data) {
	return uni.setStorageSync('baseUrl', data)
}

export default util;
