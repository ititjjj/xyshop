
import xyEmpty from './xy-empty/xy-empty.vue';
import xyList from './xy-list/xy-list.vue';
import xyListItem from './xy-list-item/xy-list-item.vue';
import xyLoading from './xy-loading/xy-loading.vue';
import xyModal from './xy-modal/xy-modal.vue';
import xyNotice from './xy-notice/xy-notice.vue';
import xyPicker from './xy-picker/xy-picker.vue';
import xyPost from './xy-post/xy-post.vue';
import xyRadioGroup from './xy-radio-group/xy-radio-group.vue';
import xyCheckboxGroup from './xy-checkbox-group/xy-checkbox-group.vue';
import xySendVerify from './xy-send-verify/xy-send-verify.vue';
import xyShare from './xy-share/xy-share.vue';
import xyTag from './xy-tag/xy-tag.vue';
import xyUploadImage from './xy-upload-image/xy-upload-image.vue';
import xybuilderFormItem from './xybuilder/xybuilder-form-item.vue';
import xybuilderForm from './xybuilder/xybuilder-form.vue';
import xybuilderList from './xybuilder/xybuilder-list.vue';

const components = {
    xyEmpty,
    xyList,
    xyListItem,
    xyLoading,
    xyModal,
    xyNotice,
    xyPicker,
    xyPost,
    xyRadioGroup,
    xyCheckboxGroup,
    xySendVerify,
    xyShare,
    xyTag,
    xyUploadImage,
    xybuilderFormItem,
    xybuilderForm,
    xybuilderList
}

export {
    xyEmpty,
    xyList,
    xyListItem,
    xyLoading,
    xyModal,
    xyNotice,
    xyPicker,
    xyPost,
    xyRadioGroup,
    xyCheckboxGroup,
    xySendVerify,
    xyShare,
    xyTag,
    xyUploadImage,
    xybuilderFormItem,
    xybuilderForm,
    xybuilderList
}

// 支持Vue.use全局引入
const install = function(Vue, options) {
	components.map(component => Vue.component(component.name, component))
}

/* 支持使用标签的方式引入,uni-app项目基本不需要 */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
	install,
	...components
}
