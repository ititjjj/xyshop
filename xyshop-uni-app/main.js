/**
 * +----------------------------------------------------------------------
 * | InitAdmin/vue-admin [ InitAdmin渐进式模块化通用后台 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018-2019 http://initadmin.net All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * +----------------------------------------------------------------------
 * | Author: jry <ijry@qq.com>
 * +----------------------------------------------------------------------
*/

import Vue from 'vue';
import App from './App';
import store from './store';
Vue.prototype.$store = store;
import util from './libs/util';
Vue.prototype.util = util;

Vue.config.productionTip = false;

App.mpType = 'app';

// filter
Vue.filter('formatTime', function(time, format) {
    return util.formatDate(time, format);
});

const app = new Vue({
	store,
	...App
});
app.$mount();
export default app;
