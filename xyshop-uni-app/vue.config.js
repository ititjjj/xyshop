module.exports = {
    configureWebpack: {
        resolve: {
            symlinks: false // 防止小程序软链接解析真实目录出错
        }
    }
}
