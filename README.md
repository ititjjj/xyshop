# XYShopB2B2C多商户新零售电商系统
> 由南京星意信息科技有限公司开发，且长期保持更新，采用成熟且先进的技术框架开发。

## 介绍
B2B2C多商户新零售电商系统，支持小程序商城/iOS商城/安卓商城/PC商城，支持多店铺管理，品类管理，商品管理，订单管理，运费模板，同城配送，秒杀，拼团，直播，短视频，预售，砍价等功能。

如需了解源码，请联系QQ：598821125  手机：13282171975

## XYSHop微信客服扫码即可咨询无需通过好友申请
![输入图片说明](xyshop-uni-app/static/iShot_2022-06-28_20.33.35.png)


## 软件架构
后端基于ThinkPHP6.0最新版本开发，前端移动端基于uni-app开发，可以适配H5/微信小程序/iOS/安卓等多个移动端平台，PC端基于Vue3+ElementPlus开发。



## 在线演示
【H5】[https://xyshop.jiangruyi.com/h5](https://xyshop.jiangruyi.com/h5)

【小程序】![输入图片说明](https://cdn.jiangruyi.com/sbase/image/gh_2c3df4afcecf_430.jpg)

【商户管理后台】[https://xyshop.jiangruyi.com/eadmin](https://xyshop.jiangruyi.com/eadmin)

【管理总后台】[https://xyshop.jiangruyi.com/admin](https://xyshop.jiangruyi.com/admin)


## 体验账号

前台/商户管理后台/平台后台统一用此账号

账号：jry

密码：Windows10

## 二次开发支持

使用文档：即将上线

接口文档：即将上线

数据库文档：即将上线

部署文档：即将上线



